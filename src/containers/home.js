import React from 'react';

const PageHome = () => {
  return (
    <div className="columns page-home">
      <div className="column col-sm-12 col-md-8">
        <section className="section home">
          <h1>
            Welcome to Friday Lunch
          </h1>
          <p>
            Here is a typical lunch break in my office
          </p>
          <blockquote>
            <div>
              We exit the office building as a group
            </div>
            <div>
              "So... what's for lunch?"
            </div>
            <div>
              Followed by a great deal of <em>umming</em> <em>and ahhing</em>
            </div>
          </blockquote>
          <p>
            With this problem in mind, I created Friday Lunch with the intention
            of learning <u>ReactJS</u> and <u>FireBase</u> while also putting a stop to indecisive lunch breaks!
          </p>
        </section>
        <section className="section home-guide">
          <p>As a guest, please feel free to register and explore. Alternatively you may use the following guest credentials:</p>
          <blockquote>
            UN: guest@example.com <br/>
            PW: password
          </blockquote>
        </section>
      </div>
    </div>
  );
}

export default PageHome;

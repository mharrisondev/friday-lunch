import React from 'react';
// eslint-disable-next-line
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import LoadingState from '../components/loading-state.js';
import { DateTime } from 'luxon';

class PageLunchResults extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      resultsList: null
    }
  }
  componentDidMount() {
    let database = this.props.database;
    let existingFridays = database.ref('fridays');

    // query for existing fridays
    existingFridays.once('value', (dataSnapshot) => {
      let snapshot = dataSnapshot.val();

      let listOfFridays = [];
      let dateNow = DateTime.local();
      let votingEndsTime = dateNow.plus({days: (7 + 5 - dateNow.weekday) % 7}).startOf('day').plus({'hours': 12});

      for (let date in snapshot) {

        let pastLunch = DateTime.fromISO(date).plus({'hours': 12, 'seconds': 1});

        if (pastLunch < votingEndsTime) {
          listOfFridays.push({
            date: date,
            humanizeDate: DateTime.fromISO(date).toLocaleString(DateTime.DATE_FULL)
          });
        }
      }

      this.setState({
        resultsList: listOfFridays,
        loading: false
      });

    });
  }
  componentWillUnmount() {
    // cancel any requests
    this.props.database.ref('fridays').off();
  }
  render() {
    if (!this.state.loading) {
      return (
        <section className="previous-lunches">
          <div className="columns page-lunch-list">
            <div className="column col-12">
              <h1>Previous Lunches</h1>
            </div>
          </div>
          <div className="columns">
            <div className="column col-12">
              <ul>
                {this.state.resultsList.map((friday) => {
                  let fridayUrl = `lunch/${friday.date}`;
                  return (
                    <li key={friday.date}><Link to={fridayUrl}>{friday.humanizeDate}</Link></li>
                  )
                })}
              </ul>
            </div>
          </div>
        </section>
      );
    } else {
      return (<LoadingState />);
    }

  }
}

export default PageLunchResults;

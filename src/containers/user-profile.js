import React from 'react';
import UserFormChangeEmail from '../components/user-form-change-email';
import UserFormChangePassword from '../components/user-form-change-password';
import UserFormChangeName from '../components/user-form-change-name';
import UserFormChangeAvatar from '../components/user-form-change-avatar';

const PageUserProfile = (props) => {
  return (
    <div className="columns page-user-profile">
      <div className="column col-12">
        <h1>
          Manage your details
        </h1>
      </div>
      <div className="column col-6 col-mr-auto">
        <UserFormChangeName user={props.user} refreshUser={props.refreshUser} database={props.database} />
        <UserFormChangeAvatar user={props.user} refreshUser={props.refreshUser} database={props.database} storage={props.storage} />
        <UserFormChangeEmail user={props.user} refreshUser={props.refreshUser} database={props.database} />
        <UserFormChangePassword user={props.user} refreshUser={props.refreshUser} firebase={props.firebase} />
      </div>
    </div>
  );
}

export default PageUserProfile;

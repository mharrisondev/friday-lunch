import React from 'react';
import { DateTime } from 'luxon';
import LoadingState from '../components/loading-state.js';
// eslint-disable-next-line
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

class PageLunchResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      fridayDate: null,
      fridayData: null
    }
  }
  componentWillMount() {
    // get date from dynamic url
    let urlDate = window.location.pathname.split('/')[2];
    this.setState({fridayDate: urlDate});
  }
  componentDidMount() {
    let database = this.props.database;
    let fridayData = database.ref('fridays').child(this.state.fridayDate);

    // query for existing fridays
    fridayData.once('value', (dataSnapshot) => {
      let snapshot = dataSnapshot.val();
      let votesArray = [];

      for (var key in snapshot.votes) {
        if (snapshot.votes.hasOwnProperty(key)) {
          let voteCount = Object.keys(snapshot.votes[key]).length;

          votesArray.push({
            lunchSuggestion: key,
            votes: snapshot.votes[key],
            voteCount: voteCount
          });
        }
      }

      // sort votesArray in order of vote count
      votesArray.sort(function(a, b) {
        let aCount = a.voteCount;
        let bCount = b.voteCount;
        if (aCount < bCount) { return  1; }
        if (aCount > bCount) { return -1; }
        return 0;
      });

      this.setState({
        fridayData: snapshot.votes,
        votesArray: votesArray,
        loading: false
      });
    });
  }
  render() {
    let humanizeDate = DateTime.fromISO(this.state.fridayDate).toLocaleString({weekday: 'long', month: 'long', day: '2-digit'});
    let tempWarning = {color: 'red'};

    if (!this.state.loading) {
      if (this.state.votesArray.length) {
        let resultsElements = this.state.votesArray.map((suggestionWithVotes, index) => {
          let rank = index + 1;

          return (
            <div key={suggestionWithVotes.lunchSuggestion}>
              #{rank} - {suggestionWithVotes.lunchSuggestion} ({suggestionWithVotes.voteCount} votes)
            </div>
          );
        });
        return (
          <section className="lunch-result">
            <div className="columns page-lunch-event">
              <div className="column col-12">
                <h1>Results for {humanizeDate}</h1>
                {resultsElements}
              </div>
              <div className="column col-12 mt-2">
                <Link to="/previous-lunches">Back to Previous Lunches</Link>
              </div>
            </div>
          </section>
        );
      } else {
        return (<span style={tempWarning}>No lunch suggestiosn for this date</span>);
      }

    } else {
      return (<LoadingState />);
    }
  }
}

export default PageLunchResult;

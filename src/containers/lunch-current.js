import React from 'react';
import LunchFormVote from '../components/lunch-vote-form.js';
import VotesDisplay from '../components/votes-display.js';
import LoadingState from '../components/loading-state.js';
import Countdown from '../components/countdown.js';
import { DateTime } from 'luxon';

class PageLunchCurrent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      date: null,
      fridayState: null,
      votingAllowed: null,
      voteEndTime: null,
      winningSuggestions: null,
      userHash: null
    }
  }
  componentWillMount() {
    let database = this.props.database;
    let dt = DateTime.local();
    let dateFriday = dt.plus({days: (7 + 5 - dt.weekday) % 7}).startOf('day').toISODate();
    let votingEndsTime = dt.plus({days: (7 + 5 - dt.weekday) % 7}).startOf('day').plus({'hours': 12});

    /*
      potential refined solution
      let d = dt.plus({days: (7 + 5 - dt.weekday) % 7});
      dt.plus({days: d > 0 ? d : 7});
    */

    if (dt < votingEndsTime) {
      this.setState({
        votingAllowed: true,
        voteEndTime: votingEndsTime
      });
    } else {
      this.setState({
        votingAllowed: false,
        voteEndTime: votingEndsTime
      });
    }

    // query for existing friday ref
    let fridayRef = database.ref('fridays').child(dateFriday);
    fridayRef.once('value', (dataSnapshot) => {
      let snapshot = dataSnapshot.val();
      // If no Lunch DB record found
      // todo refactor- this is not D.R.Y
      if (!snapshot || snapshot === null) {
        console.log("setup Friday record");
        fridayRef.set({
          date: dateFriday,
        }).then( () => {
          console.log('setup Friday - success');
          fridayRef.on('value', (snapshot) => {
            let fridaySnapshot = snapshot.val();

            if (!fridaySnapshot) {
              console.warn("setup Friday failed");
              return;
            }

            this.setState({
              date: fridaySnapshot.date,
              fridayState: fridaySnapshot,
              loading: false
            });
          });
        }).catch( () => {
          console.log('setup Friday failed');
        });
      // query existing record
      } else {
        console.log("query existing Friday");
        fridayRef.on('value', (snapshot) => {
          let fridaySnapshot = snapshot.val();

          if (!fridaySnapshot) {
            console.warn("Friday query failed");
            return;
          }

          // set current #1 suggestion
          let winningSuggestion = [];
          let winningSuggestions = [];
          let existingVotes = fridaySnapshot.votes;
          let highestCount;

          if (existingVotes) {
            for (var key in existingVotes) {
              if (existingVotes.hasOwnProperty(key)) {
                let voteCount = Object.values(existingVotes[key]).length;
                winningSuggestions.push({suggestion: key, count: voteCount});
              }
            }

            // sort based on count
            winningSuggestions.sort(function(a, b) {
              let aCount = a.count;
              let bCount = b.count;
              if (aCount < bCount) { return  1; }
              if (aCount > bCount) { return -1; }
              return 0;
            });

            highestCount = winningSuggestions[0].count;
            // check if more than one suggestion tied for lead
            winningSuggestions.forEach(suggestion => {
              if (suggestion.count === highestCount) {
                winningSuggestion.push(suggestion);
              }
            });
          }

          this.setState({
            date: fridaySnapshot.date,
            fridayState: fridaySnapshot,
            winningSuggestions: winningSuggestion,
            loading: false
          });
        });
      }
    });

    // query for all users and add to state for reference
    database.ref('users').on('value', (usersSnapshot) => {
      let allUsers = usersSnapshot.val();
      this.setState({userHash: allUsers});
    });

  }
  componentWillUnmount() {
    // cancel any requests
    this.props.database.ref('fridays').off();
  }
  render() {
    if (!this.state.loading) {
      if (!this.state.fridayState) {
        return (
          <div className="empty">
            <div className="empty-icon">
              <i className="fa fa-close"></i>
            </div>
            <p className="empty-title h5">No Friday Available</p>
            <p className="empty-subtitle">Matt needs to fix his code</p>
          </div>
        );
      }

      let humanizeDate = DateTime.fromISO(this.state.fridayState.date).toLocaleString(DateTime.DATE_HUGE);
      let winningElement;
      let winningSuggestion = this.state.winningSuggestions;
      let lunchVoteFormEle;

      if (winningSuggestion) {
        if (winningSuggestion.length > 1) {
          winningElement = (
            <div>
              <span>This weeks {this.state.votingAllowed ? 'leading' : 'winning'} suggestions: </span>
              <span className="text-secondary">
                {winningSuggestion.map(s => {
                  return `${s.suggestion}`;
                }).join(', ')}
              </span>
              <span> tied with <span className="text-secondary">{winningSuggestion[0].count}</span> votes</span>
            </div>
          );
        } else if (winningSuggestion.length === 1) {
          winningElement = (
            <span>This weeks {this.state.votingAllowed ? 'leading' : 'winning'} suggestion: <span className="text-secondary">{winningSuggestion[0].suggestion}</span> with <span className="text-secondary">{winningSuggestion[0].count}</span> votes</span>
          );
        } else {
          winningElement = (
            <span>Currently no suggestions for this Friday</span>
          );
        }
      }

      if (this.state.votingAllowed) {
        lunchVoteFormEle = <LunchFormVote user={this.props.user} database={this.props.database} />
      } else {
        lunchVoteFormEle = <span className="text-primary-dark">Voting closed for this week</span>
      }

      return (
        <section className="current-lunch">
          <div className="columns page-lunch-list">
            <div className="column col-12 mb-2">
              <h1 className="">{humanizeDate}</h1>
              <div>
                <Countdown
                  fridayDate={this.state.fridayState.date}
                  votingAllowed={this.state.votingAllowed}
                  voteEndTime={this.state.voteEndTime} />
              </div>
              <div className="winning-element">
                {winningElement}
              </div>
            </div>
            <div className="column col-12 mb-2">
              <h3>Suggestions</h3>
              <div className="columns">
                <VotesDisplay
                  votes={this.state.fridayState.votes}
                  date={this.state.date}
                  votingAllowed={this.state.votingAllowed}
                  userHash={this.state.userHash}
                  user={this.props.user}
                  database={this.props.database}
                  storage={this.props.storage} />
              </div>
            </div>
            <div className="column col-xs-12 col-sm-6 col-md-6 col-4 mb-2">
              <h3>Make a suggestion</h3>
              <div className="card">
                <div className="card-body">
                  {lunchVoteFormEle}
                </div>
              </div>
            </div>
          </div>
        </section>
      );
    } else {
      return (<LoadingState />);
    }
  }
}

export default PageLunchCurrent;

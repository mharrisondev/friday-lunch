import React from 'react';
import RegistrationForm from '../components/registration-form';

const PageRegister = (props) => {
  return (
    <div className="columns page-register">
      <div className="column col-12 text-center">
        <h1 className="title">
          Sign up to Friday Lunch
        </h1>
        <p className="subtitle">
          Register securely with <a href="https://firebase.google.com/products/auth/" target="_blank" rel="noopener noreferrer">Firebase</a>
        </p>
      </div>
      <div className="column col-4 col-mx-auto">
        <RegistrationForm firebase={props.firebase} database={props.database} refreshUser={props.refreshUser} />
      </div>
    </div>
  );
}

export default PageRegister;

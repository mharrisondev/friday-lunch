import React from 'react';
// import { BrowserRouter as Route, Link } from 'react-router-dom';

class Page404 extends React.Component {
  render() {
    return (
      <div className="columns page-404">
        <div className="column col-12">
          <h1>Page not found</h1>
        </div>
      </div>
    );
  }
}

export default Page404;

import React from 'react';
import LoginForm from '../components/login-form';

const PageLogin = (props) => {
  return (
    <div className="columns page-login">
      <div className="column col-12 text-center">
        <h1 className="title">
          Log-in to Friday Lunch
        </h1>
        <p className="subtitle">
          Sign in securely with <a href="https://firebase.google.com/products/auth/" target="_blank" rel="noopener noreferrer">Firebase</a>
        </p>
      </div>
      <div className="column col-4 col-mx-auto">
        <LoginForm firebase={props.firebase} />
      </div>
    </div>
  );
}

export default PageLogin;

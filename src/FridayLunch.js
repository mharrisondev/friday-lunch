import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import * as FireBase from 'firebase';
// Pages
import PageHome from './containers/home.js';
import PageLogIn from './containers/login.js';
import PageRegister from './containers/register.js';
import PageUserProfile from './containers/user-profile.js';
import PageLunchCurrent from './containers/lunch-current.js';
import PageLunchResults from './containers/lunch-results.js';
import PageLunchResult from './containers/lunch-result.js';
// Components
import NavigationPrimary from './components/navigation-primary.js';
import Footer from './components/footer.js';
import ReactGA from 'react-ga';
ReactGA.initialize('UA-119526652-1');

const config = {
  apiKey: "AIzaSyB8uCAOOkFrkEfwxiaBSkF94qUKwLLH4Us",
  authDomain: "friday-lunch-b7e72.firebaseapp.com",
  databaseURL: "https://friday-lunch-b7e72.firebaseio.com",
  projectId: "friday-lunch-b7e72",
  storageBucket: "friday-lunch-b7e72.appspot.com",
  messagingSenderId: "1058239378659"
}

const firebase = FireBase.initializeApp(config);
const database = FireBase.database();
const storage = FireBase.storage().ref();

class FridayLunch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      displayHero: true
    };

    this.dismissSplash = this.dismissSplash.bind(this);
    this.refreshUser = this.refreshUser.bind(this);

    firebase.auth().onAuthStateChanged( user => {
      if (user) {
        console.log('User signed in');
        this.setState({user: user});
        this.refreshUser();
      } else {
        console.log('User signed out');
        this.setState({user: user});
      }
    });
  }
  componentDidMount() {
    this.updateGoogleAnalytics(window.location.pathname);
  }
  updateGoogleAnalytics(path) {
    console.log('updateGoogleAnalytics()', path);
    ReactGA.pageview(path);
  }
  refreshUser() {
    return new Promise((resolve, reject) => {
      // console.log('refreshUser()');
      if (this.state.user && this.state.user.photoURL) {
        // pre-registered user
        this.state.user.reload().then( () => {
          this.setState({user: this.state.user});

          // create/update user entry in 'users' db
          let userDbRef = database.ref('users').child(`${this.state.user.uid}`);

          // if user has displayPicture
          storage.child(this.state.user.photoURL).getDownloadURL().then( (url) => {
            userDbRef.set({
              displayName: this.state.user.displayName,
              email: this.state.user.email,
              photoURL: this.state.user.photoURL || null,
              photoURLCached: url || null,
              uid: this.state.user.uid,
            }).then( () => {
              resolve();
              // console.log('refreshUser() -> Updated');
            }).catch( (error) => {
              console.warn(error);
            })
          });
        });
      } else {
        // get current user
        let currentUser = firebase.auth().currentUser;
        let userDbRef = database.ref('users').child(`${currentUser.uid}`);

        userDbRef.set({
          displayName: currentUser.displayName || currentUser.email,
          email: currentUser.email,
          uid: currentUser.uid,
        }).then( () => {
          resolve();
          // console.log('refreshUser() -> Updated');
        }).catch( (error) => {
          console.warn(error);
        })
      }
    });
  }
  logOut() {
    firebase.auth().signOut().then(function() {
      console.log('User signed out');
    }).catch(function(error) {
      console.log(error);
    });
  }
  dismissSplash = function(){
    this.setState({displayHero: false});
  }
  render() {
    return (
      <Router>
        <div className="friday-lunch-component">
          <div className={"home__splash-hero " + (this.state.displayHero ? 'show' : 'hide')} onClick={this.dismissSplash}>
            <div className={"home__splash-hero__title-container" + (this.state.displayHero ? ' show' : ' hide')}>
              <h1>Friday Lunch</h1>
            </div>
          </div>
          <div className="container grid-lg primary-container">
            <div className="columns">
              <div className="column col-12">
                <NavigationPrimary user={this.state.user} logout={this.logOut} storage={storage} />
              </div>
              <div className="column col-12">
                <div className="router-container mb-2">
                  <Route exact path="/" component={() => (
                    <PageHome user={this.state.user}
                      firebase={firebase}
                      database={database}
                      storage={storage} />
                  )} />
                  <Route exact path="/login" render={() => (
                    this.state.user ? (<Redirect to="/"/>) : (
                      <PageLogIn
                        user={this.state.user}
                        firebase={firebase}
                        database={database}
                        storage={storage} />
                    ))}/>
                  <Route exact path="/register" render={() => (
                    this.state.user ? (<Redirect to="/user-profile"/>) : (
                      <PageRegister
                        user={this.state.user}
                        refreshUser={this.refreshUser}
                        firebase={firebase}
                        database={database}
                        storage={storage} />
                    ))}/>
                  <Route exact path="/user-profile" render={() => (
                    this.state.user ? (
                      <PageUserProfile
                        user={this.state.user}
                        refreshUser={this.refreshUser}
                        firebase={firebase}
                        database={database}
                        storage={storage} />
                    ) : (<Redirect to="/"/>)
                  )}/>
                  <Route exact path="/lunch" render={() => (
                    this.state.user ? (
                      <PageLunchCurrent
                        user={this.state.user}
                        firebase={firebase}
                        database={database}
                        storage={storage} />
                    ) : (<Redirect to="/login"/>)
                  )}/>
                  <Route exact path="/lunch/:id" render={() => (
                    this.state.user ? (
                      <PageLunchResult
                        user={this.state.user}
                        firebase={firebase}
                        database={database}
                        storage={storage} />
                    ) : (<Redirect to="/login"/>)
                  )}/>
                  <Route exact path="/previous-lunches" render={() => (
                    this.state.user ? (
                      <PageLunchResults
                        user={this.state.user}
                        firebase={firebase}
                        database={database}
                        storage={storage} />
                    ) : (<Redirect to="/login"/>)
                  )}/>
                </div>
              </div>
              <div className="column col-12">
                <Footer />
              </div>
            </div>
          </div>
        </div>
      </Router>
    );
  }
}

export default FridayLunch;

import React from 'react';

class UserChip extends React.Component {
  constructor(props) {
    super(props);
    this.updateState = this.updateState.bind(this);
    this.state = {
      displayName: null,
      photoURL: null
    }
  }
  componentDidMount() {
    this.updateState();
  }
  componentWillReceiveProps() {
    this.updateState();
  }
  updateState() {
    if (this.props.user.photoURLCached) {
      this.setState({photoURL: this.props.user.photoURLCached});
    } else if (this.props.user.photoURL) {
      let avatarUrl = this.props.storage.child(this.props.user.photoURL);
      avatarUrl.getDownloadURL().then( (url) => {
        this.setState({photoURL: url});
      });
    } else {
      // default if none specified
      this.setState({photoURL: process.env.PUBLIC_URL + 'fridayLunchDefaultAvatar.png'})
    }
    if (this.props.user.displayName) {
      this.setState({displayName: this.props.user.displayName});
    }
  }
  render() {
    let chipText;

    if (this.state.displayName != null) {
      chipText = this.state.displayName;
    } else {
      chipText = this.props.user.email;
    }

    return (
      <div className="chip">
        <img src={this.state.photoURL} className="avatar avatar-sm" alt="" />
        {chipText}
      </div>
    );
  }
}

export default UserChip;

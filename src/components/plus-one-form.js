import React from 'react';
import { DateTime } from 'luxon';

class PlusoneForm extends React.Component {
  constructor(props) {
    super(props);
    this.checkUserMayVote = this.checkUserMayVote.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      userMayVote: true,
      loading: false,
      errorPrompt: '',
      successPrompt: ''
    };
  }
  componentDidMount() {
    this.checkUserMayVote();
  }
  componentWillReceiveProps() {
    this.checkUserMayVote();
  }
  checkUserMayVote() {
    // console.log("plus-one-form - checkUserMayVote()");
    let votes = this.props.suggestionWithVotes.votes;
    if (votes) {
      let existingVotes = Object.keys(this.props.suggestionWithVotes.votes);
      if (existingVotes.includes(this.props.user.uid)) {
        this.setState({userMayVote: false});
        return;
      } else {
        this.setState({userMayVote: true});
      }
    }
  }
  handleSubmit(event) {
    event.preventDefault();
    this.setState({loading: true});

    let submitDate = DateTime.local().toISODate().toString();
    let submitTime = DateTime.local().toISOTime().toString();
    let submitDateTime = `${submitDate}T${submitTime}`;
    let userIdentifier = this.props.user.displayName || this.props.user.email;
    let userEmail = this.props.user.email;
    let userUID = this.props.user.uid;

    this.props.database.ref('fridays').child(`${this.props.date}/votes/${this.props.suggestionWithVotes.lunchSuggestion}/${userUID}`)
    .set({
      submitDate: submitDate,
      submitTime: submitTime,
      submitDateTime: submitDateTime,
      userDisplayName: userIdentifier,
      userEmail: userEmail,
      userUID: userUID,
      suggestedFirst: false
    }, ()=> {
      this.setState({
        loading: false,
        userMayVote: false,
        successPrompt: 'Vote submitted successfully',
        errorPrompt: ''
      });
      console.log('PlusoneForm submit success');
    })
    .catch( (error) => {
      //todo
    });
  }
  render() {
    if (this.props.date && this.props.suggestionWithVotes) {
      if (!this.state.loading) {
        if(!this.state.userMayVote) {
          return (
            <button className="btn btn-success disabled">
              <i className="fa fa-check"></i>
            </button>
          );
        } else {
          return (
            <button className="btn btn-success" onClick={this.handleSubmit}>
              + 1
            </button>
          );
        }
      } else {
        return (
          <button className="btn btn-success">
            <i className="fa fa-cog fa-spin fa-fw"></i>
          </button>
        );
      }
    }
  }
}

export default PlusoneForm;

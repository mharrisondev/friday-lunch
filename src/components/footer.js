import React from 'react';

const Footer = () => {
  return (
    <footer>
      <hr />
      <div className="container">
        <div className="columns">
          <div className="column col-12 col-mx-auto">
            <div className="has-text-centered">
              <strong><i className="fa fa-cutlery"></i> Friday Lunch</strong> by
              <a href="https://gitlab.com/mharrisondev" target="_blank" rel="noopener noreferrer"> <i className="fa fa-gitlab"></i> mharrisondev </a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;

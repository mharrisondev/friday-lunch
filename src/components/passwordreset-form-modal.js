import React from 'react';

class PasswordresetFormModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {'email' : '', 'errorPrompt': ''};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  handleSubmit(event) {
    event.preventDefault();
    this.setState({submitButtonLoading: true});
    this.props.firebase.auth().sendPasswordResetEmail(this.state.email)
    .then(() => {
      this.setState({submitButtonLoading: false});
      this.props.closeModal();
    })
    .catch( (error) => {
      this.setState({submitButtonLoading: false});
      this.setState({errorPrompt: error.message});
    });
  }
  render() {
    return (
      <div className="modal active" id="password-reset-modal">
        <a href="#close" className="modal-overlay" aria-label="Close" onClick={this.props.closeModal}>&nbsp;</a>
        <div className="modal-container">
          <div className="modal-header">
            <a href="#close" className="btn btn-clear float-right" aria-label="Close" onClick={this.props.closeModal}>&nbsp;</a>
            <div className="modal-title h5">Reset password</div>
          </div>
          <div className="modal-body">
            <div className="content">
            <p>
              {this.props.message}
            </p>
              <form onSubmit={this.handleSubmit}>
                <div className={"form-group " + (this.state.errorPrompt ? 'has-error' : '')}>
                  <label className="form-label" htmlFor="emailInput">Email</label>
                  <input className="form-input" type="email" id="emailInput" name="email" placeholder="Email" value={this.state.email} onChange={this.handleChange} />
                  {this.state.errorPrompt ? <p className="form-input-hint">{this.state.errorPrompt}</p> : ''}
                </div>
                <div>
                  <button type="submit" className={"btn btn-primary " + (this.state.submitButtonLoading ? 'loading' : '')}>Submit</button>
                </div>
              </form>
            </div>
          </div>
          <div className="modal-footer">
            &nbsp;
          </div>
        </div>
      </div>
    );
  }
}

export default PasswordresetFormModal;

import React from 'react';

class RegistrationForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {'username': '', 'password': '', passwordConfirm: '', 'registrationErrorMessage': ''};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount() {
    this._isMounted = true;
  }
  componentWillUnmount() {
     this._isMounted = false;
  }
  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  handleSubmit(event) {
    event.preventDefault();
    this.setState({submitButtonLoading: true, registrationErrorMessage: ''});
    // check if passwords match
    if (this.state.password === this.state.passwordConfirm) {
      this.props.firebase.auth().createUserWithEmailAndPassword(this.state.username, this.state.password)
      .then(() => {
        return this.props.refreshUser();
      }).then(() => {
        if (this._isMounted) {
          this.setState({submitButtonLoading: false});
        }
      }).catch((error) => {
        if (this._isMounted) {
          this.setState({submitButtonLoading: false, registrationErrorMessage: error.message});
        }
      });
    } else {
      if (this._isMounted) {
        this.setState({submitButtonLoading: false, registrationErrorMessage: 'Passwords do not match'});
      }
    }
  }
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className={"form-group " + (this.state.registrationErrorMessage ? 'has-error' : '')}>
          <label className="form-label" htmlFor="emailInput">Email</label>
          <input className="form-input" type="email" id="emailInput" name="username" placeholder="Email" value={this.state.username} onChange={this.handleChange} />
          <label className="form-label" htmlFor="passwordInput">Password</label>
          <input className="form-input" type="password" id="passwordInput" name="password" placeholder="Password" value={this.state.password} onChange={this.handleChange} />
          <label className="form-label" htmlFor="passwordConfirmInput">Confirm Password</label>
          <input className="form-input" type="password" id="passwordConfirmInput" name="passwordConfirm" placeholder="Confirm password" value={this.state.passwordConfirm} onChange={this.handleChange} />
          {this.state.registrationErrorMessage ? <p className="form-input-hint">{this.state.registrationErrorMessage}</p> : ''}
        </div>
        <div>
          <button type="submit" className={"btn btn-primary " + (this.state.submitButtonLoading ? 'loading' : '')}>Submit</button>
        </div>
      </form>
    );
  }
}

export default RegistrationForm;

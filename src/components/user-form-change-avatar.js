import React from 'react';
import UserAvatar from '../components/user-avatar';

class UserFormChangeAvatar extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.state = {
      editing: false,
      submitButtonLoading: false,
      errorPrompt: '',
      successPrompt: '',
      photoURL: this.props.user.photoURL || '',
    };
  }
  handleEditClick() {
    // reset form state
    this.setState({editing: !this.state.editing, errorPrompt: null, successPrompt: null, photoURL: this.props.user.photoURL});
  }
  handleChange(event) {
    let value = event.target.value;
    let name = event.target.name;
    if (event.target.type === 'file') {
      value = event.target.files[0];
    }

    this.setState({
      [name]: value
    });
  }
  handleSubmit(event) {
    event.preventDefault();

    if (this.state.photoURL) {
      let newAvatarRef = this.props.database.ref('/users').child(this.props.user.uid);

      newAvatarRef.once('value').then( snapshot => {
        let fileRef = this.props.storage.child('userAvatars/' + snapshot.key);
        let file = this.state.photoURL;
        this.setState({photoURL: 'userAvatars/' + snapshot.key});
        fileRef.put(file).then( snapshot => {
          // TODO: this seems messy, empty promise?
        });
      }).then( () => {
        this.props.user.updateProfile({
          photoURL: this.state.photoURL
        });
      })
    }

    this.setState({submitButtonLoading: true});
    this.props.user.updateProfile({
      photoURL: this.state.photoURL
    })
    .then(() => {
      return this.props.refreshUser();
    }).then(() => {
      this.setState({submitButtonLoading: false, successPrompt: 'Successfully updated'});
    }).catch((error) => {
      this.setState({submitButtonLoading: false, errorPrompt: error.message});
    });
  }
  render() {
    if (this.state.editing) {
      return (
        <div>
          <form onSubmit={this.handleSubmit} className="mb-2">
            <div className={"form-group " + (this.state.errorPrompt ? 'has-error' : '') + (this.state.successPrompt ? 'has-success' : '')}>
              <label className="form-label" htmlFor="avatarInput">Avatar Upload</label>
              <input className="form-input" type="file" id="avatarInput" name="photoURL" accept="image/*" onChange={this.handleChange} />
              {this.state.errorPrompt ? (<p className="form-input-hint is-error">{this.state.errorPrompt}</p>) : null}
              {this.state.successPrompt ? <p className="form-input-hint is-success">{this.state.successPrompt}</p> : ''}
            </div>
            <div className="mb-2">
              <UserAvatar user={this.props.user} storage={this.props.storage} avatarSize='avatar-xl' />
            </div>
            <div>
              <button type="submit" className={"btn btn-primary mr-1 " + (this.state.submitButtonLoading ? 'loading' : '')}>Submit</button>
              <button onClick={this.handleEditClick} className="btn btn-info mr-1">
                <i className="fa fa-close"></i> Close
              </button>
            </div>
          </form>
        </div>
      );
    } else {
      return (
        <div className="mb-1">
          <button onClick={this.handleEditClick} className="btn btn-info mr-1">
            <i className="fa fa-pencil"></i>
          </button>
          <strong>Display Picture</strong>
        </div>
      );
    }
  }
}

export default UserFormChangeAvatar;

import React from 'react';
import { DateTime } from 'luxon';

class Countdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      votingAllowed: this.props.votingAllowed,
      voteEndTime: this.props.voteEndTime,
      timeRemaining: null
    };
  }
  componentDidMount() {
    this.timerTick = setInterval( () => this.tick(), 1000 );
  }
  componentWillUnmount() {
    clearInterval(this.timerTick);
  }
  tick() {
    let timeCurrent = DateTime.local();
    let timeEnd = this.state.voteEndTime;
    let remaining = timeEnd.diff(timeCurrent, ['days', 'hours', 'minutes', 'seconds']);

    this.setState({
      timeRemaining: remaining
    });
  }
  render() {
    let countdownElement;

    if (this.state.timeRemaining) {
      let remainingDays;
      let remainingHours = this.state.timeRemaining.hours > 10 ? `${Math.round(this.state.timeRemaining.hours)}` : `0${Math.round(this.state.timeRemaining.hours)}`;
      let remainingMinutes = this.state.timeRemaining.minutes > 10 ? `${Math.round(this.state.timeRemaining.minutes)}` : `0${Math.round(this.state.timeRemaining.minutes)}`;
      let remainingSeconds = this.state.timeRemaining.seconds > 10 ? `${Math.round(this.state.timeRemaining.seconds)}` : `0${Math.round(this.state.timeRemaining.seconds)}`;

      if (this.state.timeRemaining.days > 0) {
        remainingDays = this.state.timeRemaining.days <= 1 ? `${this.state.timeRemaining.days} day,` : `${this.state.timeRemaining.days} days,`;
      } else {
        remainingDays = '';
      }

      let remainingString = `${remainingDays} ${remainingHours}:${remainingMinutes}:${remainingSeconds}`;

      if (this.state.votingAllowed) {
        countdownElement = (
          <div className="mb-2">
            Voting ends in: {remainingString}
          </div>
        );
      } else {
        countdownElement = (
          <div className="mb-2">
            <p className="mb-0 text-primary-dark">
              Voting has ended for this Friday <br />
              Try again next week!
            </p>
          </div>
        );
      }

      return (
        countdownElement
      );
    } else {
      return (<div className="mb-2">&nbsp;</div>);
    }
  }
}

export default Countdown;

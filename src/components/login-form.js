import React from 'react';
import PasswordresetFormModal from '../components/passwordreset-form-modal.js';

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      'username' : '',
      'password' : '',
      'errorPrompt': '',
      'successPrompt': '',
      'modalPrompt': false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  handleSubmit(event) {
    event.preventDefault();
    this.setState({submitButtonLoading: true});
    this.props.firebase.auth().signInWithEmailAndPassword(this.state.username, this.state.password)
    .then(() => {
      this.setState({submitButtonLoading: false, successPrompt: 'Successfully updated'});
    })
    .catch( (error) => {
      this.setState({submitButtonLoading: false});
      this.setState({errorPrompt: error.message});
    });
  }
  openModal() {
    this.setState({modalPrompt: true});
  }
  closeModal(){
    this.setState({modalPrompt: false, errorPrompt: null});
  }
  render() {
    let modalPromptEle;
    if (this.state.modalPrompt) {
      modalPromptEle = (<PasswordresetFormModal closeModal={this.closeModal} firebase={this.props.firebase} message='You will recieve an email shortly after submitting' />);
    } else {
      modalPromptEle = (null);
    }
    return (
      <div>
      <form onSubmit={this.handleSubmit} className="mb-1">
        <div className={"form-group " + (this.state.errorPrompt ? 'has-error' : '')}>
          <label className="form-label" htmlFor="emailInput">Email</label>
          <input className="form-input" type="email" id="emailInput" name="username" placeholder="Email" value={this.state.username} onChange={this.handleChange} />
          <label className="form-label" htmlFor="passwordInput">Password</label>
          <input className="form-input" type="password" id="passwordInput" name="password" placeholder="Password" value={this.state.password} onChange={this.handleChange} />
          {this.state.errorPrompt ? <p className="form-input-hint">{this.state.errorPrompt}</p> : ''}
          {this.state.successPrompt ? <p className="form-input-hint">{this.state.successPrompt}</p> : ''}
        </div>
        <div>
          <button type="submit" className={"btn btn-primary " + (this.state.submitButtonLoading ? 'loading' : '')}>Submit</button>
        </div>
      </form>
        <div className="">
          <button onClick={this.openModal} className="btn btn-sm btn-anchor">Forgot your password?</button>
          {modalPromptEle}
        </div>
      </div>
    );
  }
}

export default LoginForm;

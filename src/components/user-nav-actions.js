import React from 'react';
// eslint-disable-next-line
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import UserChip from '../components/user-chip.js';

class UserNavActions extends React.Component {
  render() {
    let userManagementElement;

    if (this.props.user) {
      userManagementElement = (
        <div className="logged-in">
          <Link className="user-chip btn btn-link" to="/user-profile">
            <UserChip user={this.props.user} storage={this.props.storage} />
          </Link>
          <button className="btn btn-primary" onClick={this.props.logout}>Log out</button>
        </div>
      );
    } else {
      userManagementElement = (
        <div className="logged-out">
          <span className="has-text-grey-dark">Logged out</span>
        </div>
      );
    }

    return (
      <div className="user-nav-actions">
        {userManagementElement}
      </div>
    );
  }
}

export default UserNavActions;

import React from 'react';

class UserAvatar extends React.Component {
  constructor(props) {
    super(props);
    this.updatePhoto = this.updatePhoto.bind(this);
    this.state = {
      displayName: this.props.user.displayName || '',
      photoURL: this.props.user.photoURL || '',
      avatarSize: this.props.avatarSize || ''
    }
  }
  updatePhoto() {
    if (this.props.user.photoURL) {
      let avatarUrl = this.props.storage.child(this.props.user.photoURL);
      avatarUrl.getDownloadURL().then( (url) => {
        this.setState({photoURL: url});
      });
    }
  }
  componentDidMount() {
    this.updatePhoto();
  }
  componentWillReceiveProps() {
    this.updatePhoto();
  }
  render() {
    let user = this.props.user;
    let userInitial;
    let avatarElement;

    if (user.displayName != null) {
      userInitial = user.displayName.slice(0, 1) || '';
    } else {
      userInitial = 'FL';
    }

    if (this.state.photoURL) {
      avatarElement = <img src={this.state.photoURL} alt="" />;
    } else {
      avatarElement = null;
    }

    return (
      <figure className={"avatar " + this.state.avatarSize} data-initial={userInitial}>
        {avatarElement}
      </figure>
    );
  }
}

export default UserAvatar;

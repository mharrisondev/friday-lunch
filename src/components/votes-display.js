import React from 'react';
import VoteDisplay from '../components/vote-display.js';

class VotesDisplay extends React.Component {
  render() {
    let votesElement;
    let votesArray;
    let votes;
    let votesFromProps = this.props.votes;

    if (votesFromProps) {
      votesArray = [];

      for (var key in votesFromProps) {
        if (votesFromProps.hasOwnProperty(key)) {
          votesArray.push({
            lunchSuggestion: key,
            votes: votesFromProps[key]
          });
        }
      }
      console.log("votes-display has votes");
    } else {
      console.log("votes-display no votes");
    }

    if (votesArray && votesArray.length > 0) {
      votes = [];

      votesArray.map((suggestion) => {
        return suggestion.votes;
      }).forEach(suggestionWithVotes => {
        for (var key in suggestionWithVotes) {
          if (suggestionWithVotes.hasOwnProperty(key)) {
            votes.push(suggestionWithVotes[key]);
          }
        }
      });

      votesElement = votesArray.map(suggestionWithVotes => {
        return (
          <VoteDisplay
            key={suggestionWithVotes.lunchSuggestion}
            suggestionWithVotes={suggestionWithVotes}
            date={this.props.date}
            votingAllowed={this.props.votingAllowed}
            userHash={this.props.userHash}
            user={this.props.user}
            database={this.props.database}
            storage={this.props.storage} />
        );
      });
    } else {
      votesElement = (
        <div className="column col-12">
          <div className="empty">
            <div className="empty-icon">
              <i className="fa fa-close"></i>
            </div>
            <p className="empty-title h5">Currently no votes</p>
            <p className="empty-subtitle">Make a suggestion <i className="fa fa-thumbs-o-up"></i></p>
          </div>
        </div>
      );
    }

    return (
      votesElement
    );
  }
}

export default VotesDisplay;

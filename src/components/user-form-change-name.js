import React from 'react';

class UserFormChangeName extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.state = {
      editing: false,
      submitButtonLoading: false,
      errorPrompt: '',
      successPrompt: '',
      displayName: this.props.user.displayName || ''
    };
  }
  handleEditClick() {
    // reset form state
    this.setState({editing: !this.state.editing, errorPrompt: null, successPrompt: null, displayName: this.props.user.displayName});
  }
  handleChange(event) {
    let value = event.target.value;
    let name = event.target.name;
    if (event.target.type === 'file') {
      value = event.target.files[0];
    }

    this.setState({
      [name]: value
    });
  }
  handleSubmit(event) {
    event.preventDefault();

    this.setState({submitButtonLoading: true});
    this.props.user.updateProfile({
      displayName: this.state.displayName
    })
    .then(() => {
      return this.props.refreshUser();
    }).then(() => {
      this.setState({submitButtonLoading: false, successPrompt: 'Successfully updated'});
    }).catch((error) => {
      this.setState({submitButtonLoading: false, errorPrompt: error.message});
    });
  }
  render() {
    if (this.state.editing) {
      return (
        <div>
          <form onSubmit={this.handleSubmit} className="mb-2">
            <div className={"form-group " + (this.state.errorPrompt ? 'has-error' : '') + (this.state.successPrompt ? 'has-success' : '')}>
              <label className="form-label" htmlFor="displaynameInput">Display Name</label>
              <input className="form-input" type="text" id="displaynameInput" name="displayName" placeholder="What do we call you?" value={this.state.displayName} onChange={this.handleChange} />
              {this.state.errorPrompt ? (<p className="form-input-hint is-error">{this.state.errorPrompt}</p>) : null}
              {this.state.successPrompt ? <p className="form-input-hint is-success">{this.state.successPrompt}</p> : ''}
            </div>
            <div>
              <button type="submit" className={"btn btn-primary mr-1 " + (this.state.submitButtonLoading ? 'loading' : '')}>Submit</button>
              <button onClick={this.handleEditClick} className="btn btn-info mr-1">
                <i className="fa fa-close"></i> Close
              </button>
            </div>
          </form>
        </div>
      );
    } else {
      return (
        <div className="mb-1">
          <button onClick={this.handleEditClick} className="btn btn-info mr-1">
            <i className="fa fa-pencil"></i>
          </button>
          <strong>Name:</strong> {this.props.user.displayName}
        </div>
      );
    }
  }
}

export default UserFormChangeName;

import React from 'react';

const LoadingState = () => {
  return (
    <div className="loading-state-component text-center">
      <i className="fa fa-cog fa-spin fa-3x fa-fw"></i>
      <span className="sr-only">Loading...</span>
    </div>
  );
}

export default LoadingState;

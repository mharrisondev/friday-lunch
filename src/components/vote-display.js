import React from 'react';
import { DateTime } from 'luxon';
import LoadingState from '../components/loading-state.js';
import UserChip from '../components/user-chip.js';
import PlusoneForm from '../components/plus-one-form.js'

class VoteDisplay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    }
  }
  render() {
    let initialVote;
    let suggestionWithVotes = this.props.suggestionWithVotes;
    let userVotesArray = Object.values(suggestionWithVotes.votes);
    let humanizeInitialDate;
    let endorsedVotesArray = [];
    let initialUserDetails = {};

    userVotesArray.sort(function(a, b) {
      let aDate = Date.parse(a.submitDateTime);
      let bDate = Date.parse(b.submitDateTime);
      if (aDate > bDate) { return  1; }
      if (aDate < bDate) { return -1; }
      return 0;
    });

    initialVote = userVotesArray[0];

    humanizeInitialDate = DateTime.fromISO(`${initialVote.submitDateTime}`)
      .toLocaleString({weekday: 'short', month: 'short', day: '2-digit', hour: '2-digit', minute: '2-digit'});

    userVotesArray.slice(1, userVotesArray.length).forEach( userVote => {
      // with userUID get data from userHash prop
      let userDetails = this.props.userHash[`${userVote.userUID}`];

      return endorsedVotesArray.push({
        displayName: userDetails.displayName,
        email: userDetails.email,
        photoURL: userDetails.photoURL,
        photoURLCached: userDetails.photoURLCached
      });
    });

    initialUserDetails = {
      displayName: this.props.userHash[`${initialVote.userUID}`].displayName || this.props.userHash[`${initialVote.userUID}`].email,
      email: this.props.userHash[`${initialVote.userUID}`].email,
      photoURL: this.props.userHash[`${initialVote.userUID}`].photoURL,
      photoURLCached: this.props.userHash[`${initialVote.userUID}`].photoURLCached
    }

    if (!this.state.loading) {
      let endorsedVotesEle;
      let plusOneFormEle;

      let endorsedVotesChips = endorsedVotesArray.map(endorsedVote => {
        return (
          <UserChip
            key={endorsedVote.displayName}
            user={endorsedVote}
            storage={this.props.storage} />
        );
      });

      if (endorsedVotesArray.length >= 1) {
        endorsedVotesEle = (
          <div className="endorsed-votes">
            Endorsed by:
            <div>
              {endorsedVotesChips}
            </div>
          </div>
        );
      } else {
        endorsedVotesEle = null;
      }

      if (this.props.votingAllowed) {
        plusOneFormEle = (
          <div className="plus-one">
            <span>Vote for this suggestion: </span>
            <PlusoneForm
              suggestionWithVotes={suggestionWithVotes}
              date={this.props.date}
              database={this.props.database}
              user={this.props.user} />
          </div>
        );
      } else {
        plusOneFormEle = null;
      }

      if (suggestionWithVotes) {
        return(
          <div className="column col-xs-12 col-sm-6 col-md-6 col-4 vote-display" key={suggestionWithVotes.lunchSuggestion}>
            <div className="card mb-1">
              <div className="card-header">
                <div className="card-title h5 text-secondary">
                  {suggestionWithVotes.lunchSuggestion}
                </div>
                <div className="card-subtitle">
                <div>
                  <span>Suggested by:</span> <UserChip storage={this.props.storage} user={initialUserDetails} />
                </div>
                <div>
                  <small className="text-gray">Submitted: {humanizeInitialDate}</small>
                </div>
                </div>
              </div>
              <div className="card-body">
                <div>
                  {endorsedVotesEle}
                </div>
              </div>

              <div className="card-footer">
                <div>
                  <span className="text-secondary">{userVotesArray.length} {userVotesArray.length > 1 ? 'Votes' : 'Vote'}</span>
                </div>
                {plusOneFormEle}
              </div>
            </div>
          </div>
        );
      }
    } else {
      return (
        <div className="column col-xs-12 col-sm-6 col-md-6 col-4" key={suggestionWithVotes.lunchSuggestion}>
          <div className="card mb-1">
            <div className="card-body">
              <LoadingState user={this.props.user} />
            </div>
          </div>
        </div>
      );
    }
  }
}

export default VoteDisplay;

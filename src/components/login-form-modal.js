import React from 'react';

class LoginFormModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {'username' : '', 'password' : '', 'errorPrompt': ''};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  handleSubmit(event) {
    event.preventDefault();
    this.setState({submitButtonLoading: true});
    this.props.firebase.auth().signInWithEmailAndPassword(this.state.username, this.state.password)
    .then(() => {
      this.setState({submitButtonLoading: false});
      this.props.closeModal();
    })
    .catch( (error) => {
      this.setState({submitButtonLoading: false});
      this.setState({errorPrompt: error.message});
    });
  }
  render() {
    return (
      <div className="modal active" id="login-modal">
        <a href="#close" className="modal-overlay" aria-label="Close">&nbsp;</a>
        <div className="modal-container">
          <div className="modal-header">
            <a href="#close" className="btn btn-clear float-right" aria-label="Close">&nbsp;</a>
            <div className="modal-title h5">Log in</div>
          </div>
          <div className="modal-body">
            <div className="content">
            <p>
              {this.props.message}
            </p>
              <form onSubmit={this.handleSubmit}>
                <div className={"form-group " + (this.state.errorPrompt ? 'has-error' : '')}>
                  <label className="form-label" htmlFor="emailInput">Email</label>
                  <input className="form-input" type="email" id="emailInput" name="username" placeholder="Email" value={this.state.username} onChange={this.handleChange} />
                  <label className="form-label" htmlFor="passwordInput">Password</label>
                  <input className="form-input" type="password" id="passwordInput" name="password" placeholder="Password" value={this.state.password} onChange={this.handleChange} />
                  {this.state.errorPrompt ? <p className="form-input-hint">{this.state.errorPrompt}</p> : ''}
                </div>
                <div>
                  <button type="submit" className={"btn btn-primary " + (this.state.submitButtonLoading ? 'loading' : '')}>Submit</button>
                </div>
              </form>
            </div>
          </div>
          <div className="modal-footer">
            &nbsp;
          </div>
        </div>
      </div>
    );
  }
}

export default LoginFormModal;

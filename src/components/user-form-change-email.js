import React from 'react';
import LoginFormModal from '../components/login-form-modal.js';

class UserFormChangeEmail extends React.Component {
  constructor(props) {
    super(props);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      editing: false,
      submitButtonLoading: false,
      errorPrompt: '',
      successPrompt: '',
      requiresAuth: false,
      email: this.props.user.email || ''
    };
  }
  componentWillReceiveProps(props) {
    if (props.user.email) {
      this.setState({email: props.user.email});
    }
  }
  handleEditClick() {
    // reset form state
    this.setState({editing: !this.state.editing, errorPrompt: null, successPrompt: null, email: this.props.user.email});
  }
  handleChange(event) {
    const value = event.target.value;
    const name = event.target.name;
    this.setState({
      [name]: value
    });
  }
  closeModal(){
    this.setState({requiresAuth: false, errorPrompt: null});
  }
  handleSubmit(event) {
    event.preventDefault();
    this.setState({submitButtonLoading: true});
    this.props.user.updateEmail(this.state.email)
    .then(() => {
      return this.props.refreshUser();
    }).then(() => {
      this.setState({submitButtonLoading: false, successPrompt: 'Successfully updated'});
    }).catch((error) => {
      this.setState({submitButtonLoading: false, errorPrompt: error.message});

      if (error.code === "auth/requires-recent-login") {
        this.setState({requiresAuth: true});
      }
    });
  }
  render() {
    if (this.state.editing) {
      if (!this.state.requiresAuth) {
        return (
          <div>
            <form onSubmit={this.handleSubmit} className="mb-2">
              <div className={"form-group " + (this.state.errorPrompt ? 'has-error' : '') + (this.state.successPrompt ? 'has-success' : '')}>
                <label className="form-label" htmlFor="emailInput">Email address</label>
                <input className="form-input" type="email" id="emailInput" name="email" placeholder="Email address" value={this.state.email} onChange={this.handleChange} />
                {this.state.errorPrompt ? <p className="form-input-hint is-error">{this.state.errorPrompt}</p> : ''}
                {this.state.successPrompt ? <p className="form-input-hint is-success">{this.state.successPrompt}</p> : ''}
              </div>
              <div>
                <button type="submit" className={"btn btn-primary mr-1 " + (this.state.submitButtonLoading ? 'loading' : '')}>Submit</button>
                <button onClick={this.handleEditClick} className="btn btn-info mr-1">
                  <i className="fa fa-close"></i> Close
                </button>
              </div>
            </form>
          </div>
        );
      } else {
        return (
          <LoginFormModal firebase={this.props.firebase} closeModal={this.closeModal} message='Please reauthenticate to update Email' />
        );
      }
    } else {
      return (
        <div className="mb-1">
          <button onClick={this.handleEditClick} className="btn btn-info mr-1">
            <i className="fa fa-pencil"></i>
          </button>
          <strong>Email:</strong> {this.props.user.email}
        </div>
      );
    }
  }
}

export default UserFormChangeEmail;

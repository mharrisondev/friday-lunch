import React from 'react';
import LoginFormModal from '../components/login-form-modal.js';

class UserFormChangePassword extends React.Component {
  constructor(props) {
    super(props);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.state = {
      editing: false,
      submitButtonLoading: false,
      errorPrompt: '',
      successPrompt: '',
      requiresAuth: false,
      password: '',
      passwordConfirm: ''
    };
  }
  handleEditClick() {
    // reset form state
    this.setState({editing: !this.state.editing, errorPrompt: null, successPrompt: null, password: '', passwordConfirm: ''});
  }
  handleChange(event) {
    const value = event.target.value;
    const name = event.target.name;
    this.setState({
      [name]: value
    });
  }
  closeModal(){
    this.setState({requiresAuth: false, errorPrompt: null});
  }
  handleSubmit(event) {
    event.preventDefault();
    this.setState({submitButtonLoading: true});
    // ensure passwords match
    if (this.state.password === this.state.passwordConfirm) {
      this.props.user.updatePassword(this.state.password)
      .then(() => {
        return this.props.refreshUser();
      }).then(() => {
        this.setState({submitButtonLoading: false, successPrompt: 'Successfully updated'});
      }).catch((error) => {
        this.setState({submitButtonLoading: false, errorPrompt: error.message});
        // if requires re-auth
        if (error.code === "auth/requires-recent-login") {
          this.setState({requiresAuth: true});
        }
      });
    } else {
      // passwords do not match
      this.setState({errorPrompt: 'Passwords do not match'});
      this.setState({submitButtonLoading: false});
    }
  }
  render() {
    if (this.state.editing) {
      if (!this.state.requiresAuth) {
        return (
          <div>
            <form onSubmit={this.handleSubmit} className="mb-2">
              <div className={"form-group " + (this.state.errorPrompt ? 'has-error' : '') + (this.state.successPrompt ? 'has-success' : '')}>
                <label className="form-label" htmlFor="passwordInput">Password</label>
                <input className="form-input" type="password" id="passwordInput" name="password" placeholder="" value={this.state.password} onChange={this.handleChange} />
                <label className="form-label" htmlFor="passwordInputConfirm">Confirm Password</label>
                <input className="form-input" type="password" id="passwordInputConfirm" name="passwordConfirm" placeholder="" value={this.state.passwordConfirm} onChange={this.handleChange} />
                {this.state.errorPrompt ? <p className="form-input-hint is-error">{this.state.errorPrompt}</p> : ''}
                {this.state.successPrompt ? <p className="form-input-hint is-success">{this.state.successPrompt}</p> : ''}
              </div>
              <div>
                <button type="submit" className={"btn btn-primary mr-1 " + (this.state.submitButtonLoading ? 'loading' : '')}>Submit</button>
                <button onClick={this.handleEditClick} className="btn btn-info mr-1">
                  <i className="fa fa-close"></i> Close
                </button>
              </div>
            </form>
          </div>
        );
      } else {
        return (
          <LoginFormModal firebase={this.props.firebase} closeModal={this.closeModal} message='Please reauthenticate to update password' />
        );
      }
    } else {
      return (
        <div className="mb-1">
          <button onClick={this.handleEditClick} className="btn btn-info mr-1">
            <i className="fa fa-pencil"></i>
          </button>
           <strong>Password</strong>
        </div>
      );
    }
  }
}

export default UserFormChangePassword;

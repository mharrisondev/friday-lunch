import React from 'react';
import { DateTime } from 'luxon';

class LunchFormVote extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      date: null,
      hasVotes: null,
      lunchSuggestion: '',
      submitButtonLoading: false,
      errorPrompt: '',
      successPrompt: '',
      requiresAuth: false,
    };
  }
  componentDidMount() {
    let database = this.props.database;
    let dt = DateTime.local();
    let dateFriday = dt.plus({days: (7 + 5 - dt.weekday) % 7});
    const fridaysRef = database.ref('fridays').child(`${dateFriday.toISODate()}`);

    fridaysRef.on('value', (snapshot) => {
      let hasVotes = snapshot.child("votes").exists();
      let snapshotVal = snapshot.val();

      return this.setState({date: snapshotVal.date, hasVotes: hasVotes});
    });
  }
  submitLunchSuggestion() {
    console.log("submitLunchSuggestion()");
    let database = this.props.database;
    let dt = DateTime.local();
    let submitDate = dt.toISODate().toString();
    let submitTime = dt.toISOTime().toString();
    let submitDateTime = `${submitDate}T${submitTime}`;

    if (this.state.lunchSuggestion.length > 1) {

      database.ref('fridays').child(`${this.state.date}/votes/`).once("value")
        .then( (snapshot) => {

          let existingVoteQuery;

          if (snapshot.val()) {
            existingVoteQuery = Object.keys(snapshot.val()).map(key => {
              return key.toLowerCase();
            });
          } else {
            existingVoteQuery = [];
          }

          // if submission matches suggestion already made
          if (existingVoteQuery.includes((this.state.lunchSuggestion).toLowerCase())) {
            this.setState({submitButtonLoading: false, errorPrompt: 'Suggestion exists, Try +1 the existing suggestion'});
          } else {
            console.log('submitLunchSuggestion() no existing results');
            // Create new suggestion if none exist
            let user = this.props.user.displayName || this.props.user.email;
            let userEmail = this.props.user.email;
            let userUID = this.props.user.uid;

            database.ref('fridays').child(`${this.state.date}/votes/${this.state.lunchSuggestion}/${this.props.user.uid}`)
            .set({
              submitDate: submitDate,
              submitTime: submitTime,
              submitDateTime: submitDateTime,
              userDisplayName: user,
              userEmail: userEmail,
              userUID: userUID,
              suggestedFirst: true
            }, ()=> {
              this.setState({
                lunchSuggestion: '',
                submitButtonLoading: false,
                successPrompt: 'Vote submitted successfully',
                errorPrompt: ''
              });
              console.log('submitLunchSuggestion success');
            });
          }
        })
        .catch( (error) => {
          console.warn(error);
          //todo
        });
    }
  }
  handleChange(event) {
    const value = event.target.value;
    const name = event.target.name;
    this.setState({
      [name]: value
    });
  }
  handleSubmit(event) {
    event.preventDefault();
    this.setState({submitButtonLoading: true});
    this.submitLunchSuggestion();
  }
  render() {
    if (!this.state.requiresAuth) {
      return (
        <div>
          <form onSubmit={this.handleSubmit} className="mb-2">
            <div className={"form-group " + (this.state.errorPrompt ? 'has-error' : '') + (this.state.successPrompt ? 'has-success' : '')}>
              <label className="form-label" htmlFor="lunchSuggestionInput">Suggest a lunch location</label>
              <input className="form-input" type="text" id="lunchSuggestionInput" name="lunchSuggestion" placeholder="Where to?" value={this.state.lunchSuggestion} onChange={this.handleChange} />
              {this.state.errorPrompt ? <p className="form-input-hint is-error">{this.state.errorPrompt}</p> : ''}
              {this.state.successPrompt ? <p className="form-input-hint is-success">{this.state.successPrompt}</p> : ''}
            </div>
            <div>
              <button type="submit" className={"btn btn-success mr-1 " + (this.state.submitButtonLoading ? 'loading' : '')}>Submit</button>
            </div>
          </form>
        </div>
      );
    }
  }
}

export default LunchFormVote;

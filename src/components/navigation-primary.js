import React from 'react';
// eslint-disable-next-line
import { BrowserRouter as Router, Route, NavLink, Link } from 'react-router-dom';
import UserNavActions from '../components/user-nav-actions';

class ComponentNavigationPrimary extends React.Component {
  render() {
    let loginStateElements;

    if (this.props.user) {
      // if logged in
      loginStateElements = (
        <section className="navbar-section">
          <NavLink className="btn btn-link" activeClassName="active" to="/lunch">Lunch!</NavLink>
          <NavLink className="btn btn-link" activeClassName="active" to="/previous-lunches">Previous Lunches</NavLink>
        </section>
      );
    } else {
      // if logged out
      loginStateElements = (
        <section className="navbar-section">
          <NavLink className="btn btn-link" activeClassName="active" to="/login">Log-in</NavLink>
          <NavLink className="btn btn-link" activeClassName="active" to="/register">Sign Up</NavLink>
        </section>
      );
    }

    return (
      <div>
        <header className="navbar">
          <section className="navbar-section">
            <Link className="navbar-brand btn btn-link mr-2" to="/">
              <i className="fa fa-cutlery"></i> Friday Lunch
            </Link>
          </section>
          {loginStateElements}
          <section className="navbar-section" style={{flex: 'none'}}>
            <UserNavActions user={this.props.user} logout={this.props.logout} storage={this.props.storage} />
          </section>
        </header>
      </div>
    );
  }
}

export default ComponentNavigationPrimary;

import React, { Component } from 'react';
import FridayLunch from './FridayLunch';
import './FridayLunch.css';

class App extends Component {
  render() {
    return (
      <FridayLunch />
    );
  }
}

export default App;
